import './Input.css';

import { useState, useEffect } from 'react';

export default function Input({
	onChange,
	inputType = 'text',
	inputName,
	placeholdetText,
	labelText,
	...props
}) {
	const [inputId, setInputId] = useState();
	useEffect(() => {
		setInputId(Math.ceil(Date.now() * Math.random()));
	}, []);

	return (
		<>
			<label htmlFor={inputId}>{labelText}</label>
			<input
				className='input'
				type={inputType}
				id={inputId}
				name={inputName}
				placeholder={placeholdetText}
				onChange={onChange}
				{...props}
			></input>
		</>
	);
}
