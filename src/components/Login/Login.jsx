import axios from 'axios';
import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';

export default function Login() {
	const [message, setMessage] = useState('');
	const navigate = useNavigate();

	const handleSubmit = (e) => {
		e.preventDefault();
		const email = e.target.email.value.trim();
		const password = e.target.password.value.trim();

		axios
			.post('http://localhost:3000/login', {
				email,
				password,
			})
			.then((response) => {
				e.target.reset();
				console.log(response);
				window.localStorage.setItem('token', response.data.result);
				window.localStorage.setItem('user', JSON.stringify(response.data.user));
				navigate('/courses');
			})
			.catch((error) => {
				console.dir(error);
				setMessage(error.response.data.result);
			});
	};
	return (
		<div className='container'>
			<div className='registration-form-wrap'>
				<h2>Login</h2>
				<form className='registration-form' onSubmit={handleSubmit}>
					<Input
						labelText='Email'
						placeholdetText='Enter email'
						inputName='email'
						inputType='text'
						required
					></Input>
					<Input
						labelText='Password'
						placeholdetText='Enter password'
						inputName='password'
						inputType='password'
						minLength='6'
						required
					></Input>
					<div className='form-wrap-btn '>
						<Button buttonText='Login' buttonType='submit'></Button>
					</div>
					<div className='message'>{message && <p>{message}</p>}</div>
				</form>
				<p>
					If you don't have an account you can
					<Link to='/registration'> Registration</Link>
				</p>
			</div>
		</div>
	);
}
