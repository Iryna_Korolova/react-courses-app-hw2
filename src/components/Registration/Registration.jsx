import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';

import './Registration.css';

import { useState } from 'react';

import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';

export default function Registration() {
	const [message, setMessage] = useState('');
	const navigate = useNavigate();

	const handleSubmit = (e) => {
		e.preventDefault();
		const name = e.target.name.value.trim();
		const email = e.target.email.value.trim();
		const password = e.target.password.value.trim();

		axios
			.post('http://localhost:3000/register', {
				name,
				email,
				password,
			})
			.then((response) => {
				e.target.reset();
				console.log(response);
				navigate('/login');
			})
			.catch((error) => {
				console.dir(error);
				setMessage(error.response.data.errors[0]);
			});
	};
	return (
		<div className='container'>
			<div className='registration-form-wrap'>
				<div className='message'>{message && <p>{message}</p>}</div>
				<h2>Registration</h2>
				<form className='registration-form' onSubmit={handleSubmit}>
					<Input
						labelText='Name'
						placeholdetText='Enter name'
						inputName='name'
						inputType='text'
						required
					></Input>
					<Input
						labelText='Email'
						placeholdetText='Enter email'
						inputName='email'
						inputType='text'
						required
					></Input>
					<Input
						labelText='Password'
						placeholdetText='Enter password'
						inputName='password'
						inputType='password'
						minLength='6'
						required
					></Input>
					<div className='form-wrap-btn '>
						<Button buttonText='Registration' buttonType='submit'></Button>
					</div>
				</form>
				<p>
					If you have an account you can <Link to='/login'>Login</Link>
				</p>
			</div>
		</div>
	);
}
