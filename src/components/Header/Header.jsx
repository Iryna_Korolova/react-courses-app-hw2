import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';

import './Header.css';

import { useNavigate, useLocation } from 'react-router-dom';
import { useEffect, useState } from 'react';

export default function Header() {
	const [token, setToken] = useState(null);
	const [user, setUser] = useState(null);
	const navigate = useNavigate();
	const location = useLocation();
	useEffect(() => {
		const token = window.localStorage.getItem('token');
		setToken(token);
		const user = JSON.parse(window.localStorage.getItem('user'));
		setUser(user);
		const tokenLessRoutes = ['/login', '/registration'];
		if (
			tokenLessRoutes.some((path) => location.pathname.startsWith(path)) &&
			token
		) {
			navigate('/courses', { replace: true });
		}
		const tokenRoutes = ['/courses'];
		if (
			tokenRoutes.some((path) => location.pathname.startsWith(path)) &&
			!token
		) {
			navigate('/login', { replace: true });
		}
	}, [location]);

	function logOut() {
		window.localStorage.removeItem('token');
		window.localStorage.removeItem('user');
		navigate('/login', { replace: true });
	}
	return (
		<header className='header container'>
			<div className='header-inner'>
				<Logo />
				{token && (
					<>
						<div className='header-inner'>
							<h3 className='header-heading'>{user.name}</h3>
							<Button buttonText='Logout' onClick={logOut} />
						</div>
					</>
				)}
			</div>
		</header>
	);
}
