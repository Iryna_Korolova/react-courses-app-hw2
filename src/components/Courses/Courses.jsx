import { useState } from 'react';

import { mockedCoursesList } from './../../constants';

import CourseCard from './CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';
import CreateCourse from '../CreateCourse/CreateCourse';
import CourseInfo from '../CourseInfo/CourseInfo';

import { useNavigate, Route, Routes } from 'react-router-dom';

export default function Courses() {
	const navigate = useNavigate();
	const [courses, setCourses] = useState(mockedCoursesList);
	const [searchValue, setSearchValue] = useState('');

	return (
		<main>
			<Routes>
				<Route
					path=''
					element={
						<div className='container'>
							<div className='searchbar-wrap'>
								<SearchBar setSearchValue={setSearchValue}></SearchBar>
								<Button
									buttonText='Add new course'
									onClick={() => navigate('/courses/add')}
								></Button>
							</div>
							{courses
								.filter((course) =>
									`${course.title} ${course.id}`
										.toLocaleLowerCase()
										.includes(searchValue)
								)
								.map((course) => (
									<CourseCard key={course.id} course={course}></CourseCard>
								))}
						</div>
					}
				></Route>
				<Route
					path='add'
					element={<CreateCourse setCourses={setCourses} />}
				></Route>
				<Route
					path=':courseId'
					element={<CourseInfo courses={courses}></CourseInfo>}
				></Route>
			</Routes>
		</main>
	);
}
